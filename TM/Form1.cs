﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TM
{
    public partial class Form1 : Form
    {
        dynamic loader;
        dynamic tm;
        dynamic simulator;
        System.Windows.Forms.Timer theTimer = new System.Windows.Forms.Timer();
        Type stateType = typeof(Int32);
        Type alphabetType = typeof(Char);


        public Form1()
        {
            stateType = typeof(Int32);
            alphabetType = typeof(Char);
            Type TML = typeof(TuringMachineLoader<,>);
            Type[] t = { alphabetType, stateType };
            Type loaderType = TML.MakeGenericType(t);
            loader = Activator.CreateInstance(loaderType);
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (TrasisionsFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TransisionsPath.Text = TrasisionsFileDialog.FileName;
                TransisionsValues.Text = loader.readFromFile(TrasisionsFileDialog.FileName);
            }
        }

        private void reloadLoader()
        {
            Type TML = typeof(TuringMachineLoader<,>);
            stateType = Type.GetType(StateTypeBox.Text.Trim());
            alphabetType = Type.GetType(AlphabetTypeBox.Text.Trim());
            Type[] t = { alphabetType, stateType };
            Type loaderType = TML.MakeGenericType(t);
            loader = Activator.CreateInstance(loaderType);

        }

        private void RunButton_Click(object sender, EventArgs e)
        {
            if (RunButton.Text != "Run")
            {
                theTimer.Enabled = !theTimer.Enabled;
                RunButton.Text = "Continue";
                return;
            }

            Summary.ResetText();
            Application.DoEvents();
            Type TML = typeof(TuringMachineLoader<,>);
            Type TM = typeof(TuringMachine<,>);
            Type TMS = typeof(TuringMachineSimulator<,>);

            stateType = Type.GetType(StateTypeBox.Text.Trim());
            alphabetType = Type.GetType( AlphabetTypeBox.Text.Trim());
            Type[] t = { alphabetType, stateType };
            Type loaderType = TML.MakeGenericType(t);
            Type simulatorType = TMS.MakeGenericType(t);
            Type machineType = TM.MakeGenericType(t);


            loader = Activator.CreateInstance(loaderType);
            try
            {

            
                tm = Activator.CreateInstance(machineType, loader.readTape(TapeValue.Text), loader.readStates(StatesValues.Text), loader.readTransisions(TransisionsValues.Text), Int16.Parse(CarretTextBox.Text), Convert.ChangeType(initStateBox.Text, stateType), Convert.ChangeType(blankTextBox.Text, alphabetType));
                simulator = Activator.CreateInstance(simulatorType,tm, VisualisationPanel.CreateGraphics());
            
                theTimer.Tick += new EventHandler(TimerEventProcessor);
                theTimer.Interval = Int16.Parse(DelayTextBox.Text);
                theTimer.Start();
                RunButton.Text = "Pause";
            }
            catch (Exception )
            {
                Summary.ResetText(); Summary.Text = "An error occured while parsing data";
            }
        }

        private void TimerEventProcessor(Object myObject, EventArgs myEventArgs)
        {
            if (tm.end)
            {
                theTimer.Stop(); 
                RunButton.Text = "Run";
                StringBuilder builder = new StringBuilder();
                builder.Append("*** Statistics  ***");
                builder.Append("*** Job Finished  ***");
                builder.Append("*** Current State : " + tm.currentState);
                Summary.ResetText(); Summary.Text = builder.ToString() ;
            }
            else
            {
                simulator.step();
            }
                
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (StatesFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StatesPath.Text = StatesFileDialog.FileName;
                StatesValues.Text = loader.readFromFile(StatesFileDialog.FileName);
            }
        }

        private void TapeButton_Click(object sender, EventArgs e)
        {
            if (TapeFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TapePath.Text = TapeFileDialog.FileName;
                TapeValue.Text = loader.readFromFile(TapeFileDialog.FileName);
            }
        }

        private void TapeManual_CheckedChanged(object sender, EventArgs e)
        {
            TapeValue.ReadOnly = !TapeValue.ReadOnly;
            TapePath.ReadOnly = !TapePath.ReadOnly;
        }

        private void TransisionManual_CheckedChanged(object sender, EventArgs e)
        {
            TransisionsPath.ReadOnly = !TransisionsPath.ReadOnly;
            TransisionsValues.ReadOnly = !TransisionsPath.ReadOnly;
        }

        private void StatesManual_CheckedChanged(object sender, EventArgs e)
        {
            StatesPath.ReadOnly = !StatesPath.ReadOnly;
            StatesValues.ReadOnly = !StatesValues.ReadOnly;
        }

        private void StatesValues_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void saveFileDialog2_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void saveTransisionsToXML_Click(object sender, EventArgs e)
        {
            if (saveToXMLDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                reloadLoader();
                loader.serializeToXml(saveToXMLDialog.FileName, loader.readTape(TapeValue.Text), loader.readStates(StatesValues.Text), loader.readTransisions(TransisionsValues.Text), Int32.Parse(CarretTextBox.Text), Convert.ChangeType(initStateBox.Text, stateType));
            }
        }

        private void loadFromXML_Click(object sender, EventArgs e)
        {
            if (LoadFromXMLDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                reloadLoader();
                tm = loader.deserializeFromXml(LoadFromXMLDialog.FileName);
                StringBuilder tape = new StringBuilder();
                foreach (Object obj in tm.Tape)
                {
                    tape.Append(obj.ToString());
                    tape.Append(";");
                }
                TapeValue.Text = tape.Remove(tape.Length - 1, 1).ToString();
                StringBuilder states = new StringBuilder();
                foreach (Object obj in tm.PossibleStates)
                {
                    states.Append((String)Convert.ChangeType(obj, typeof(string)));
                    states.Append(";");
                }
                StatesValues.Text = states.Remove(states.Length - 1, 1).ToString();

                StringBuilder transisions = new StringBuilder();
                foreach (dynamic obj in tm.Transisions)
                {
                    transisions.Append(obj.Key.ToString() + " ");
                    transisions.Append(obj.Value.Item1.ToString() + " ");
                    transisions.Append(obj.Value.Item2.ToString() + " ");
                    transisions.Append((obj.Value.Item3 == Direction.Right ? "r" : "l"));
                    transisions.Append(Environment.NewLine);
                }
                TransisionsValues.Text = transisions.ToString();
                CarretTextBox.Text = tm.Carret.ToString();
                initStateBox.Text = tm.currentState.ToString();
            }
        }

        private void StateTypeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            stateType = Type.GetType(StateTypeBox.Text);
        }

        private void AlphabetTypeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            alphabetType = Type.GetType(AlphabetTypeBox.Text);
        }

        private void blankTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void TapeValue_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

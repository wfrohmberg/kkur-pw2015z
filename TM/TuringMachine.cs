﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Windows.Forms;
using System.Reflection;

namespace TM
{
    public class TuringMachine<AlphabetType, StateType>
        where AlphabetType : IConvertible
        where StateType: IConvertible
    {
        public List<AlphabetType> Tape { set; get; }
        public List<StateType> PossibleStates { set; get; }
        public Dictionary<StateType, Tuple<AlphabetType, StateType, Direction>> Transisions { set; get; }
        public StateType currentState { set; get; }
        public bool end = false;
        public AlphabetType blank { set; get; }

        public int Carret { get; set; }

        public TuringMachine()
        {
            this.Tape = new List<AlphabetType>();
            this.PossibleStates = new List<StateType>();
            this.Transisions = new Dictionary<StateType, Tuple<AlphabetType, StateType, Direction>>();
            this.Carret = 1;
        }

        public TuringMachine(List<AlphabetType> Tape, List<StateType> States, Dictionary<StateType, Tuple<AlphabetType, StateType, Direction>> Transisions, int Carret, object initState, object blank)
        {
            this.Tape = Tape;
            this.PossibleStates = States;
            this.Transisions = Transisions;
            this.Carret = Carret;
            this.currentState = (StateType) initState;
            this.blank = (AlphabetType)blank;
        }

        public static TuringMachine<AlphabetType, StateType> operator +(TuringMachine<AlphabetType, StateType> tm, StateType state)
        {
            tm.PossibleStates.Add(state);
            return tm;
        }

        public static TuringMachine<AlphabetType, StateType> operator +(TuringMachine<AlphabetType, StateType> tm, Tuple<StateType, AlphabetType, StateType, Direction> transition)
        {
            tm.Transisions.Add(transition.Item1, new Tuple<AlphabetType, StateType, Direction>(transition.Item2, transition.Item3, transition.Item4));
            return tm;
        }

        public Tuple<AlphabetType, StateType, Direction> getTransision(StateType state)
        {
            try
            {
                return Transisions[state];
            }
            catch {
                end = true;
                return null;
            }
        }

        public void Step()
        {
            if (Tape[Carret] != null || !end)
            {
                Tuple<AlphabetType, StateType, Direction> data = getTransision(currentState);
                if (data == null || Tape[Carret].Equals(blank))
                {
                    end = true;
                } 
                else 
                {
                    Tape[Carret] = data.Item1;
                    currentState = data.Item2;
                    Carret += (int)data.Item3;
                }
            }
        }
    }

    public enum Direction
    {
        Left = -1, Right = 1
    }

    public class TuringMachineLoader<AlphabetType, StateType>
        where AlphabetType : IConvertible
        where StateType: IConvertible
    {

        Type alpha = typeof(AlphabetType);
        Type state = typeof(StateType);
        public List<AlphabetType> readTape(String value)
        {
            if (value == null) { 
               throw new MissingFieldException("Tape was not provided.");
            }
            StreamReader reader = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(value)));
            while (!reader.EndOfStream)
            {
                string line = reader.ReadToEnd();
                if (!string.IsNullOrEmpty(line))
                {
                    Type alpha = typeof(AlphabetType);
                    List<AlphabetType> list = new List<AlphabetType>();
                    string[] elements = line.Split(';');
                    foreach (string element in elements)
                    {
                        list.Add((AlphabetType)Convert.ChangeType(element, alpha));
                    }
                    return list;
                }
            }
            throw new MissingFieldException("Tape was not provided.");
        }
        public List<StateType> readStates(String value)
        {
            if (value == null)
            {
                throw new MissingFieldException("States were not provided.");
            }

            try
            {
                List<StateType> list = new List<StateType>();
                StreamReader reader = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(value)));
                while (!reader.EndOfStream)
                {
                    string text = reader.ReadToEnd();
                    string[] tmp = text.Split(';');
                    foreach (string t in tmp)
                    {
                        Type state = typeof(StateType);
                        list.Add((StateType)Convert.ChangeType(t.Trim(), state));
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                throw new FormatException(string.Format("Error reading states. Exception is: %s. Inner exception: %s ", ex.Message, ex.InnerException));
            }
        }
        public Dictionary<StateType, Tuple<AlphabetType, StateType, Direction>> readTransisions(String value)
        {
            if (value == null)
            {
                throw new MissingFieldException("States were not provided.");
            }

            try
            {
                StreamReader reader = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(value)));
                Dictionary<StateType, Tuple<AlphabetType, StateType, Direction>> transisions = new Dictionary<StateType, Tuple<AlphabetType, StateType, Direction>>(); ;
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    if (!string.IsNullOrEmpty(line))
                    {
                        // FORMAT : a b c d
                        // a - source state
                        // b - value to write to tape
                        // c - target state
                        // d - l or r 

                        Type state = typeof(StateType);
                        Type alpha = typeof(AlphabetType);
                        string[] temp = line.Split(' ');
                        char direction = char.Parse(temp[3]);
                        transisions.Add((StateType)Convert.ChangeType(temp[0], state), Tuple.Create((AlphabetType)Convert.ChangeType(temp[1], alpha), (StateType)Convert.ChangeType(temp[2], state), direction.Equals('r') ? Direction.Right : Direction.Left));
                    }
                }
                return transisions;
            }
            catch (Exception ex)
            {
                throw new FormatException(string.Format("Error reading transisions. Exception is: %s. Inner exception: %s ", ex.Message, ex.InnerException));
            }

        }
        public String readFromFile(String path)
        {
            StreamReader reader = new StreamReader(File.OpenRead(path));
            return reader.ReadToEnd();
        }
        // Add types
        public void serializeToXml(String path, List<AlphabetType> Tape, List<StateType> States, Dictionary<StateType, Tuple<AlphabetType, StateType, Direction>> Transisions, int Carret, object initState)
        {
            using (XmlWriter writer = XmlWriter.Create(path))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Structure");
                writer.WriteElementString("AlphabetType", alpha.ToString());
                writer.WriteElementString("StateType", state.ToString());

                StringBuilder sb = new StringBuilder();
                foreach(AlphabetType a in Tape.ToArray())
                {
                    sb.Append(a.ToString());
                    sb.Append(',');
                }
                writer.WriteElementString("Tape", sb.Remove(sb.Length-1,1).ToString() );
                writer.WriteStartElement("States");
                foreach (StateType i in States)
                {
                    writer.WriteElementString("State", i.ToString());
                }
                writer.WriteEndElement();

                writer.WriteStartElement("Transisions");
                foreach (KeyValuePair<StateType, Tuple<AlphabetType, StateType, Direction>> entry in Transisions)
                {
                    writer.WriteStartElement("Transision");
                           
                    writer.WriteAttributeString("sourceState", entry.Key.ToString());
                    writer.WriteAttributeString("write", entry.Value.Item1.ToString());
                    writer.WriteAttributeString("targetState", entry.Value.Item2.ToString());
                    writer.WriteAttributeString("direction", entry.Value.Item3.Equals(Direction.Right) ? "r" : "l");
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();

                writer.WriteElementString("Carret", Carret.ToString());

                writer.WriteElementString("InitialState", initState.ToString());

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

        }

        public TuringMachine<AlphabetType, StateType> deserializeFromXml(String path)
        {
            List<AlphabetType> tape = new List<AlphabetType>();
            List<StateType> states = new List<StateType>();
            Dictionary<StateType, Tuple<AlphabetType, StateType, Direction>> transisions = new Dictionary<StateType, Tuple<AlphabetType, StateType, Direction>>();
            int Carret = 0;
            StateType initState = default(StateType);
            Type stateType = Type.GetType("System.Int32");
            Type alphabetType = Type.GetType("System.Char");
            AlphabetType blank = default(AlphabetType);

            using (XmlReader reader = XmlReader.Create(path))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "Tape":
                                if (reader.Read())
                                {
                                    foreach (string c in reader.Value.Trim().Split(','))
                                    {
                                        tape.Add((AlphabetType)Convert.ChangeType(c, alpha));
                                    }
                                }
                                    
                                break;
                            case "State":
                                if (reader.Read()) 
                                    states.Add((StateType)Convert.ChangeType(reader.Value.Trim(), state));
                                break;
                            case "Transisions":
                                break;
                            case "Transision":
                                    Direction dir = reader["direction"] == "Right" ? Direction.Right : Direction.Left ;
                                    transisions.Add((StateType)Convert.ChangeType(reader["sourceState"], state), Tuple.Create((AlphabetType)Convert.ChangeType(reader["write"], alpha),(StateType)Convert.ChangeType(reader["targetState"], state) , dir));
                                break;
                            case "Carret":
                                if (reader.Read()) 
                                Carret = Int32.Parse(reader.Value.Trim());
                                break;
                            case "InitialState":
                                if (reader.Read()) 
                                initState = (StateType)Convert.ChangeType(reader.Value.Trim(), state);
                                break;
                            case "StateType":
                                if (reader.Read())
                                    stateType = Type.GetType(reader.Value.Trim());
                                break;
                            case "AlphabetType":
                                if (reader.Read())
                                    alphabetType = Type.GetType(reader.Value.Trim());
                                break;
                            case "blank":
                                if (reader.Read())
                                    blank = (AlphabetType)Convert.ChangeType(reader.Value.Trim(), alpha);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            if (!typeof(AlphabetType).Equals(alphabetType) || !typeof(StateType).Equals(stateType))
                throw new FormatException();
            else
            {
                return new TuringMachine<AlphabetType, StateType>(tape, states, transisions, Carret, initState, blank);
            }
        }
       

    }

    public class TuringMachineSimulator<AlphabetType, StateType>
        where AlphabetType : IConvertible
        where StateType : IConvertible
    {
        public TuringMachine<AlphabetType, StateType> tm { get; set; }
        private Graphics surface;
        Type alpha = typeof(AlphabetType);
        Type state = typeof(StateType);

        public TuringMachineSimulator(TuringMachine<AlphabetType, StateType> tm, Graphics surface)
        {
            this.tm = tm;
            this.surface = surface;
        }

        public void step()
        {
            DrawTape();
            tm.Step();
        }
             
        private void DrawTape()
        {
            Font drawFont = new Font("Arial", 10);
            SolidBrush solidbr = new SolidBrush(Color.Black);
            surface.Clear(Color.Khaki);
            for (int i = 0; i < tm.Tape.Count; i++)
            {
                AlphabetType ch = tm.Tape[i];
                if (i == tm.Carret)
                {
                    solidbr.Color = Color.Red;
                    surface.DrawLine(new Pen(solidbr), new Point(10 * i, 20), new Point(10 * i, 120));
                    StringFormat strformat = new StringFormat();
                    strformat.FormatFlags = StringFormatFlags.DirectionVertical;
                    Tuple<AlphabetType, StateType, Direction> t = tm.getTransision(tm.currentState);
                    if (t == null)
                    {
                        t = Tuple.Create(tm.blank, tm.currentState, Direction.Right);
                    }
                    surface.DrawString("Turing Machine", drawFont, new SolidBrush(Color.Black), new Point(10 * (i - 2), 40), strformat);
                    surface.DrawString(string.Format("Input state: {0}", tm.currentState), drawFont, solidbr, new Point(10 * (i + 1), 55));
                    surface.DrawString(string.Format("Input: {0}", i), drawFont, solidbr, new Point(10 * (i + 1), 70));
                    surface.DrawString(string.Format("({0},{1},{2},{3},{4})", tm.currentState, tm.Tape[tm.Carret], t.Item1, t.Item2, t.Item3), drawFont, solidbr, new Point(10 * (i + 1), 40));
                    surface.DrawString(string.Format("Output state: {0}", t.Item1), drawFont, solidbr, new Point(10 * (i + 1), 85));
                    surface.DrawString(string.Format("Output symbol: {0}", t.Item2), drawFont, solidbr, new Point(10 * (i + 1), 100));
                    surface.DrawString(string.Format("Move: {0}", t.Item3), drawFont, solidbr, new Point(10 * (i + 1), 115));

                    surface.DrawRectangle(new Pen(new SolidBrush(Color.Navy)), new Rectangle(10 * (i - 2), 40, 200, 100));

                }
                else if (ch.Equals(tm.blank))
                    solidbr.Color = Color.Blue;
                else
                    solidbr.Color = Color.Black;

                surface.DrawString(ch.ToString(), drawFont, solidbr, 10 * i, 10);
            }
        }
    }
}


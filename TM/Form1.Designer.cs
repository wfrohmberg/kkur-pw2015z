﻿namespace TM
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DelayLabel = new System.Windows.Forms.Label();
            this.TransisionsLabel = new System.Windows.Forms.Label();
            this.StatesLabel = new System.Windows.Forms.Label();
            this.DelayTextBox = new System.Windows.Forms.TextBox();
            this.TransisionsPath = new System.Windows.Forms.TextBox();
            this.StatesPath = new System.Windows.Forms.TextBox();
            this.TapeLabel = new System.Windows.Forms.Label();
            this.TapePath = new System.Windows.Forms.TextBox();
            this.TapeButton = new System.Windows.Forms.Button();
            this.StatesButton = new System.Windows.Forms.Button();
            this.TransisionsButton = new System.Windows.Forms.Button();
            this.TrasisionsFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.StatesFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.TapeFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.RunButton = new System.Windows.Forms.Button();
            this.TransisionsValues = new System.Windows.Forms.TextBox();
            this.StatesValues = new System.Windows.Forms.TextBox();
            this.TapeValue = new System.Windows.Forms.TextBox();
            this.VisualisationPanel = new System.Windows.Forms.Panel();
            this.Summary = new System.Windows.Forms.TextBox();
            this.StatesManual = new System.Windows.Forms.CheckBox();
            this.TransisionManual = new System.Windows.Forms.CheckBox();
            this.TapeManual = new System.Windows.Forms.CheckBox();
            this.CarretTextBox = new System.Windows.Forms.TextBox();
            this.CarretLabel = new System.Windows.Forms.Label();
            this.saveToXML = new System.Windows.Forms.Button();
            this.saveToXMLDialog = new System.Windows.Forms.SaveFileDialog();
            this.loadFromXML = new System.Windows.Forms.Button();
            this.LoadFromXMLDialog = new System.Windows.Forms.OpenFileDialog();
            this.initStateBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.StateTypeBox = new System.Windows.Forms.ComboBox();
            this.AlphabetTypeBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.blankTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // DelayLabel
            // 
            this.DelayLabel.AutoSize = true;
            this.DelayLabel.Location = new System.Drawing.Point(240, 169);
            this.DelayLabel.Name = "DelayLabel";
            this.DelayLabel.Size = new System.Drawing.Size(34, 13);
            this.DelayLabel.TabIndex = 0;
            this.DelayLabel.Text = "Delay";
            // 
            // TransisionsLabel
            // 
            this.TransisionsLabel.AutoSize = true;
            this.TransisionsLabel.Location = new System.Drawing.Point(337, 36);
            this.TransisionsLabel.Name = "TransisionsLabel";
            this.TransisionsLabel.Size = new System.Drawing.Size(60, 13);
            this.TransisionsLabel.TabIndex = 1;
            this.TransisionsLabel.Text = "Transisions";
            this.TransisionsLabel.Click += new System.EventHandler(this.label2_Click);
            // 
            // StatesLabel
            // 
            this.StatesLabel.AutoSize = true;
            this.StatesLabel.Location = new System.Drawing.Point(731, 36);
            this.StatesLabel.Name = "StatesLabel";
            this.StatesLabel.Size = new System.Drawing.Size(37, 13);
            this.StatesLabel.TabIndex = 2;
            this.StatesLabel.Text = "States";
            // 
            // DelayTextBox
            // 
            this.DelayTextBox.Location = new System.Drawing.Point(280, 166);
            this.DelayTextBox.Name = "DelayTextBox";
            this.DelayTextBox.Size = new System.Drawing.Size(100, 20);
            this.DelayTextBox.TabIndex = 3;
            this.DelayTextBox.Text = "1000";
            // 
            // TransisionsPath
            // 
            this.TransisionsPath.Location = new System.Drawing.Point(403, 33);
            this.TransisionsPath.Multiline = true;
            this.TransisionsPath.Name = "TransisionsPath";
            this.TransisionsPath.ReadOnly = true;
            this.TransisionsPath.Size = new System.Drawing.Size(279, 20);
            this.TransisionsPath.TabIndex = 4;
            // 
            // StatesPath
            // 
            this.StatesPath.Location = new System.Drawing.Point(801, 33);
            this.StatesPath.Multiline = true;
            this.StatesPath.Name = "StatesPath";
            this.StatesPath.ReadOnly = true;
            this.StatesPath.Size = new System.Drawing.Size(248, 20);
            this.StatesPath.TabIndex = 5;
            // 
            // TapeLabel
            // 
            this.TapeLabel.AutoSize = true;
            this.TapeLabel.Location = new System.Drawing.Point(48, 199);
            this.TapeLabel.Name = "TapeLabel";
            this.TapeLabel.Size = new System.Drawing.Size(32, 13);
            this.TapeLabel.TabIndex = 6;
            this.TapeLabel.Text = "Tape";
            // 
            // TapePath
            // 
            this.TapePath.Location = new System.Drawing.Point(118, 196);
            this.TapePath.Name = "TapePath";
            this.TapePath.ReadOnly = true;
            this.TapePath.Size = new System.Drawing.Size(279, 20);
            this.TapePath.TabIndex = 7;
            // 
            // TapeButton
            // 
            this.TapeButton.Location = new System.Drawing.Point(403, 194);
            this.TapeButton.Name = "TapeButton";
            this.TapeButton.Size = new System.Drawing.Size(52, 23);
            this.TapeButton.TabIndex = 8;
            this.TapeButton.Text = "...";
            this.TapeButton.UseVisualStyleBackColor = true;
            this.TapeButton.Click += new System.EventHandler(this.TapeButton_Click);
            // 
            // StatesButton
            // 
            this.StatesButton.Location = new System.Drawing.Point(1055, 33);
            this.StatesButton.Name = "StatesButton";
            this.StatesButton.Size = new System.Drawing.Size(26, 20);
            this.StatesButton.TabIndex = 9;
            this.StatesButton.Text = "...";
            this.StatesButton.UseVisualStyleBackColor = true;
            this.StatesButton.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // TransisionsButton
            // 
            this.TransisionsButton.Location = new System.Drawing.Point(688, 32);
            this.TransisionsButton.Name = "TransisionsButton";
            this.TransisionsButton.Size = new System.Drawing.Size(35, 21);
            this.TransisionsButton.TabIndex = 10;
            this.TransisionsButton.Text = "...";
            this.TransisionsButton.UseVisualStyleBackColor = true;
            this.TransisionsButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // TrasisionsFileDialog
            // 
            this.TrasisionsFileDialog.FileName = "Trasisions";
            this.TrasisionsFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // StatesFileDialog
            // 
            this.StatesFileDialog.FileName = "States";
            // 
            // TapeFileDialog
            // 
            this.TapeFileDialog.FileName = "Tape";
            // 
            // RunButton
            // 
            this.RunButton.Location = new System.Drawing.Point(1099, 33);
            this.RunButton.Name = "RunButton";
            this.RunButton.Size = new System.Drawing.Size(82, 153);
            this.RunButton.TabIndex = 11;
            this.RunButton.Text = "Run";
            this.RunButton.UseVisualStyleBackColor = true;
            this.RunButton.Click += new System.EventHandler(this.RunButton_Click);
            // 
            // TransisionsValues
            // 
            this.TransisionsValues.Location = new System.Drawing.Point(403, 60);
            this.TransisionsValues.Multiline = true;
            this.TransisionsValues.Name = "TransisionsValues";
            this.TransisionsValues.Size = new System.Drawing.Size(320, 126);
            this.TransisionsValues.TabIndex = 12;
            this.TransisionsValues.TabStop = false;
            this.TransisionsValues.Text = "0 x 1 r\r\n1 H 2 l\r\n2 x 3 r\r\n3 w 4 r\r\n4 w 5 r\r\n5 x 6 r\r\n6 x 1 r\r\n";
            // 
            // StatesValues
            // 
            this.StatesValues.Location = new System.Drawing.Point(801, 59);
            this.StatesValues.Multiline = true;
            this.StatesValues.Name = "StatesValues";
            this.StatesValues.Size = new System.Drawing.Size(280, 127);
            this.StatesValues.TabIndex = 13;
            this.StatesValues.Text = "0;1;2;3;4;5;6;7;8;9";
            this.StatesValues.TextChanged += new System.EventHandler(this.StatesValues_TextChanged);
            // 
            // TapeValue
            // 
            this.TapeValue.Location = new System.Drawing.Point(118, 224);
            this.TapeValue.Name = "TapeValue";
            this.TapeValue.Size = new System.Drawing.Size(539, 20);
            this.TapeValue.TabIndex = 14;
            this.TapeValue.Text = "1;1;1;1;1;1;1;1;1;1;b;b;b;b;b";
            this.TapeValue.TextChanged += new System.EventHandler(this.TapeValue_TextChanged);
            // 
            // VisualisationPanel
            // 
            this.VisualisationPanel.Location = new System.Drawing.Point(83, 247);
            this.VisualisationPanel.Name = "VisualisationPanel";
            this.VisualisationPanel.Size = new System.Drawing.Size(1098, 158);
            this.VisualisationPanel.TabIndex = 15;
            // 
            // Summary
            // 
            this.Summary.Location = new System.Drawing.Point(83, 412);
            this.Summary.Multiline = true;
            this.Summary.Name = "Summary";
            this.Summary.ReadOnly = true;
            this.Summary.Size = new System.Drawing.Size(574, 107);
            this.Summary.TabIndex = 16;
            // 
            // StatesManual
            // 
            this.StatesManual.AutoSize = true;
            this.StatesManual.Checked = true;
            this.StatesManual.CheckState = System.Windows.Forms.CheckState.Checked;
            this.StatesManual.Location = new System.Drawing.Point(734, 62);
            this.StatesManual.Name = "StatesManual";
            this.StatesManual.Size = new System.Drawing.Size(61, 17);
            this.StatesManual.TabIndex = 17;
            this.StatesManual.Text = "Manual";
            this.StatesManual.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.StatesManual.UseVisualStyleBackColor = true;
            this.StatesManual.CheckedChanged += new System.EventHandler(this.StatesManual_CheckedChanged);
            // 
            // TransisionManual
            // 
            this.TransisionManual.AutoSize = true;
            this.TransisionManual.Checked = true;
            this.TransisionManual.CheckState = System.Windows.Forms.CheckState.Checked;
            this.TransisionManual.Location = new System.Drawing.Point(340, 62);
            this.TransisionManual.Name = "TransisionManual";
            this.TransisionManual.Size = new System.Drawing.Size(61, 17);
            this.TransisionManual.TabIndex = 18;
            this.TransisionManual.Text = "Manual";
            this.TransisionManual.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TransisionManual.UseVisualStyleBackColor = true;
            this.TransisionManual.CheckedChanged += new System.EventHandler(this.TransisionManual_CheckedChanged);
            // 
            // TapeManual
            // 
            this.TapeManual.AutoSize = true;
            this.TapeManual.Checked = true;
            this.TapeManual.CheckState = System.Windows.Forms.CheckState.Checked;
            this.TapeManual.Location = new System.Drawing.Point(51, 226);
            this.TapeManual.Name = "TapeManual";
            this.TapeManual.Size = new System.Drawing.Size(61, 17);
            this.TapeManual.TabIndex = 19;
            this.TapeManual.Text = "Manual";
            this.TapeManual.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.TapeManual.UseVisualStyleBackColor = true;
            this.TapeManual.CheckedChanged += new System.EventHandler(this.TapeManual_CheckedChanged);
            // 
            // CarretTextBox
            // 
            this.CarretTextBox.Location = new System.Drawing.Point(704, 224);
            this.CarretTextBox.Name = "CarretTextBox";
            this.CarretTextBox.Size = new System.Drawing.Size(91, 20);
            this.CarretTextBox.TabIndex = 20;
            this.CarretTextBox.Text = "1";
            // 
            // CarretLabel
            // 
            this.CarretLabel.AutoSize = true;
            this.CarretLabel.Location = new System.Drawing.Point(663, 226);
            this.CarretLabel.Name = "CarretLabel";
            this.CarretLabel.Size = new System.Drawing.Size(35, 13);
            this.CarretLabel.TabIndex = 21;
            this.CarretLabel.Text = "Carret";
            this.CarretLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // saveToXML
            // 
            this.saveToXML.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.saveToXML.Location = new System.Drawing.Point(51, 28);
            this.saveToXML.Name = "saveToXML";
            this.saveToXML.Size = new System.Drawing.Size(148, 21);
            this.saveToXML.TabIndex = 22;
            this.saveToXML.Text = "Save Structure to XML";
            this.saveToXML.UseVisualStyleBackColor = true;
            this.saveToXML.Click += new System.EventHandler(this.saveTransisionsToXML_Click);
            // 
            // saveToXMLDialog
            // 
            this.saveToXMLDialog.FileName = "Struct.xml";
            // 
            // loadFromXML
            // 
            this.loadFromXML.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.loadFromXML.Location = new System.Drawing.Point(51, 55);
            this.loadFromXML.Name = "loadFromXML";
            this.loadFromXML.Size = new System.Drawing.Size(148, 21);
            this.loadFromXML.TabIndex = 23;
            this.loadFromXML.Text = "Load Structure to XML";
            this.loadFromXML.UseVisualStyleBackColor = true;
            this.loadFromXML.Click += new System.EventHandler(this.loadFromXML_Click);
            // 
            // LoadFromXMLDialog
            // 
            this.LoadFromXMLDialog.FileName = "Struct.xml";
            // 
            // initStateBox
            // 
            this.initStateBox.Location = new System.Drawing.Point(118, 166);
            this.initStateBox.Name = "initStateBox";
            this.initStateBox.Size = new System.Drawing.Size(100, 20);
            this.initStateBox.TabIndex = 25;
            this.initStateBox.Text = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 169);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Initial State";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "State Type";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "AlphabetType";
            // 
            // StateTypeBox
            // 
            this.StateTypeBox.FormattingEnabled = true;
            this.StateTypeBox.Items.AddRange(new object[] {
            "System.Char",
            "System.String",
            "System.Int32"});
            this.StateTypeBox.Location = new System.Drawing.Point(130, 80);
            this.StateTypeBox.Name = "StateTypeBox";
            this.StateTypeBox.Size = new System.Drawing.Size(121, 21);
            this.StateTypeBox.TabIndex = 30;
            this.StateTypeBox.Text = "System.Int32";
            this.StateTypeBox.SelectedIndexChanged += new System.EventHandler(this.StateTypeBox_SelectedIndexChanged);
            // 
            // AlphabetTypeBox
            // 
            this.AlphabetTypeBox.FormattingEnabled = true;
            this.AlphabetTypeBox.Items.AddRange(new object[] {
            "System.Char",
            "System.Int32"});
            this.AlphabetTypeBox.Location = new System.Drawing.Point(130, 110);
            this.AlphabetTypeBox.Name = "AlphabetTypeBox";
            this.AlphabetTypeBox.Size = new System.Drawing.Size(121, 21);
            this.AlphabetTypeBox.TabIndex = 31;
            this.AlphabetTypeBox.Text = "System.Char";
            this.AlphabetTypeBox.SelectedIndexChanged += new System.EventHandler(this.AlphabetTypeBox_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(811, 226);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 33;
            this.label4.Text = "blankCharacter";
            // 
            // blankTextBox
            // 
            this.blankTextBox.Location = new System.Drawing.Point(896, 223);
            this.blankTextBox.Name = "blankTextBox";
            this.blankTextBox.Size = new System.Drawing.Size(91, 20);
            this.blankTextBox.TabIndex = 32;
            this.blankTextBox.Text = "b";
            this.blankTextBox.TextChanged += new System.EventHandler(this.blankTextBox_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1227, 600);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.blankTextBox);
            this.Controls.Add(this.AlphabetTypeBox);
            this.Controls.Add(this.StateTypeBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.initStateBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.loadFromXML);
            this.Controls.Add(this.saveToXML);
            this.Controls.Add(this.CarretLabel);
            this.Controls.Add(this.CarretTextBox);
            this.Controls.Add(this.TapeManual);
            this.Controls.Add(this.TransisionManual);
            this.Controls.Add(this.StatesManual);
            this.Controls.Add(this.Summary);
            this.Controls.Add(this.VisualisationPanel);
            this.Controls.Add(this.TapeValue);
            this.Controls.Add(this.StatesValues);
            this.Controls.Add(this.TransisionsValues);
            this.Controls.Add(this.RunButton);
            this.Controls.Add(this.TransisionsButton);
            this.Controls.Add(this.StatesButton);
            this.Controls.Add(this.TapeButton);
            this.Controls.Add(this.TapePath);
            this.Controls.Add(this.TapeLabel);
            this.Controls.Add(this.StatesPath);
            this.Controls.Add(this.TransisionsPath);
            this.Controls.Add(this.DelayTextBox);
            this.Controls.Add(this.StatesLabel);
            this.Controls.Add(this.TransisionsLabel);
            this.Controls.Add(this.DelayLabel);
            this.Name = "Form1";
            this.Text = "TM";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label DelayLabel;
        private System.Windows.Forms.Label TransisionsLabel;
        private System.Windows.Forms.Label StatesLabel;
        private System.Windows.Forms.TextBox DelayTextBox;
        private System.Windows.Forms.TextBox TransisionsPath;
        private System.Windows.Forms.TextBox StatesPath;
        private System.Windows.Forms.Label TapeLabel;
        private System.Windows.Forms.TextBox TapePath;
        private System.Windows.Forms.Button TapeButton;
        private System.Windows.Forms.Button StatesButton;
        private System.Windows.Forms.Button TransisionsButton;
        private System.Windows.Forms.OpenFileDialog TrasisionsFileDialog;
        private System.Windows.Forms.OpenFileDialog StatesFileDialog;
        private System.Windows.Forms.OpenFileDialog TapeFileDialog;
        private System.Windows.Forms.Button RunButton;
        private System.Windows.Forms.TextBox TransisionsValues;
        private System.Windows.Forms.TextBox StatesValues;
        private System.Windows.Forms.TextBox TapeValue;
        private System.Windows.Forms.Panel VisualisationPanel;
        private System.Windows.Forms.TextBox Summary;
        private System.Windows.Forms.CheckBox StatesManual;
        private System.Windows.Forms.CheckBox TransisionManual;
        private System.Windows.Forms.CheckBox TapeManual;
        private System.Windows.Forms.TextBox CarretTextBox;
        private System.Windows.Forms.Label CarretLabel;
        private System.Windows.Forms.Button saveToXML;
        private System.Windows.Forms.SaveFileDialog saveToXMLDialog;
        private System.Windows.Forms.Button loadFromXML;
        private System.Windows.Forms.OpenFileDialog LoadFromXMLDialog;
        private System.Windows.Forms.TextBox initStateBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox StateTypeBox;
        private System.Windows.Forms.ComboBox AlphabetTypeBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox blankTextBox;

    }
}


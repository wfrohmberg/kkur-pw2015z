﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TM;
using System.Linq;
using System.Collections.Generic;

namespace TMTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void PlusOperatorStatesTest()
        {
            TuringMachine<char, Int32> tm = new TuringMachine<char, int>();
            tm += 3;
            tm += 4;
            tm += 5;
            List<Int32> list = new List<Int32>();
            list.Add(3);
            list.Add(4);
            list.Add(5);

            Assert.IsNotNull(tm.PossibleStates);
            Assert.IsTrue(Enumerable.SequenceEqual(tm.PossibleStates, list));
        }

        [TestMethod]
        public void PlusOperatorTransisionsTest()
        {
            Tuple<Int32, char, Int32, Direction> testTrans = Tuple.Create(1, 'a', 2, Direction.Right);
            Tuple<Int32, char, Int32, Direction> testTrans2 = Tuple.Create(2, 'b', 3, Direction.Right);
            TuringMachine<char, Int32> tm = new TuringMachine<char, int>();
            tm += testTrans;
            tm += testTrans2;
            Dictionary<Int32, Tuple<char, Int32, Direction>> dict = new Dictionary<Int32, Tuple<char, Int32, Direction>>();
            dict.Add(testTrans.Item1, Tuple.Create(testTrans.Item2, testTrans.Item3, testTrans.Item4));
            dict.Add(testTrans2.Item1, Tuple.Create(testTrans2.Item2, testTrans2.Item3, testTrans2.Item4));
            
            Assert.IsNotNull(tm.PossibleStates);
            Assert.IsTrue(Enumerable.SequenceEqual(tm.Transisions, dict));
        }

        [TestMethod]
        public void PlusOperatorStringStatesTest()
        {
            TuringMachine<char, string> tm = new TuringMachine<char, string>();
            tm += "hi";
            tm += "ha";
            tm += "ho";
            List<string> list = new List<string>();
            list.Add("hi");
            list.Add("ha");
            list.Add("ho");

            Assert.IsNotNull(tm.PossibleStates);
            Assert.IsTrue(Enumerable.SequenceEqual(tm.PossibleStates, list));
        }

        [TestMethod]
        public void PlusOperatorStringTransisionsTest()
        {
            Tuple<string, char, string, Direction> testTrans = Tuple.Create("hi", 'a', "ho", Direction.Right);
            Tuple<string, char, string, Direction> testTrans2 = Tuple.Create("ho", 'b', "he", Direction.Right);
            TuringMachine<char, string> tm = new TuringMachine<char, string>();
            tm += testTrans;
            tm += testTrans2;
            Dictionary<string, Tuple<char, string, Direction>> dict = new Dictionary<string, Tuple<char, string, Direction>>();
            dict.Add(testTrans.Item1, Tuple.Create(testTrans.Item2, testTrans.Item3, testTrans.Item4));
            dict.Add(testTrans2.Item1, Tuple.Create(testTrans2.Item2, testTrans2.Item3, testTrans2.Item4));

            Assert.IsNotNull(tm.PossibleStates);
            Assert.IsTrue(Enumerable.SequenceEqual(tm.Transisions, dict));
        }
    }
}
